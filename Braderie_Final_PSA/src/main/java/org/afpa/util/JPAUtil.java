package org.afpa.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	static {
		try {
			// Créer une EntityManagerFactory à partir de hibernate.cfg.xml
			factory = Persistence.createEntityManagerFactory("PersistenceUnit");
			entityManager = factory.createEntityManager();
		} catch (Throwable ex) {
			// Gestion exception
			System.err.println("Echec création SessionFactory" + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	public static EntityManager getEntityManager() {
		return entityManager;
	}
}
