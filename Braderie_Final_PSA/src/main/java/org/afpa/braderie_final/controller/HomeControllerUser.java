/**
 * 
 */
package org.afpa.braderie_final.controller;

import javax.servlet.http.HttpSession;

import org.afpa.braderie_final.service.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author samia
 *
 */

@Controller
@RequestMapping ("/users")
public class HomeControllerUser {
	@Autowired
	public ServiceUser serviceUser;
	@RequestMapping ("/")
	public String maVue (Model model) {
		
		return "../index";
	}

	@RequestMapping ("/test.do")
	public String TestConnexion (@RequestParam String username, @RequestParam String password, Model model, HttpSession maSession) {
		
		if (serviceUser.IsValidlogon(username, password)){
			//Ajouter le user au http session
			maSession.setAttribute("user", serviceUser.findByLogin(username));
			
		}
		else {
			//Renvoyer la page de login
			maSession.setAttribute("user", null);
		}
		return "redirect:../panier/";
	}
	
	





}
