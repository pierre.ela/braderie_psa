/**
 * 
 */
package org.afpa.braderie_final.controller;

import java.util.Optional;

import org.afpa.braderie_final.bean.Article;
import org.afpa.braderie_final.repository.ArticleRepository;
import org.afpa.braderie_final.service.ServiceArticle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



/**
 * @author alexo
 *
 */

@RequestMapping
@RestController
public class ControllerArticle {


	@Autowired
	private ServiceArticle hServiceArticle;

	@Autowired 
	private ArticleRepository articleRepository;

	//private final Logger log = LoggerFactory.getLogger(ControllerArticle.class);

	//Test: http://localhost:8080/hello-world
	@GetMapping(path="/hello-world")  
	public String helloWorld()  
	{  
		return "Hello World";  
	}  
	
	@GetMapping(path="/afficheCatalogue")
	public String affCat(Model model) {
		
		return "affcat";
	}

	//========================================================================
	//					CRUD ARTICLE
	//========================================================================

	// Requête GET obtenir une article par son id: http://localhost:8080/articles/5
	@GetMapping("/article/{id}")
	public @ResponseBody ResponseEntity<String> getServiceArticleById(@PathVariable String id) {
		System.out.println("DANS ControllerArticle.getServiceArticleById");
		return new ResponseEntity <String> ("Response from GET with id " + id, 
				HttpStatus.OK);
	}

	//Requête GET obtenir la liste des articles: http://localhost:8080/listearticles
	@GetMapping("/listearticles")
	public ResponseEntity<Iterable<Article>> serviceListeArticle() {
		System.out.println("DANS ControllerArticle.serviceListeArticle");
		Iterable<Article> list = (Iterable<Article>) articleRepository.findAll();
		return  new ResponseEntity<Iterable<Article>> (list, HttpStatus.FOUND) ;
	}

	//Requête pour supprimer une article: http://localhost:8080/deletearticle/5
	@DeleteMapping("/deletearticle/{id}")
	public @ResponseBody ResponseEntity<String> deleteArticleById(@PathVariable Integer id){
		hServiceArticle.deleteArticleById(id);
		return new ResponseEntity <String> ("DANS ControllerArticle.deleteArticleById " + id, 
				HttpStatus.OK);
	}

	//Requête pour créer une article en POST: http://localhost:8080/addarticle +
	//json Postman: {"description": "Portable", "marque": "IPhone", "prixunitaire": 800.00}
	@RequestMapping(value = { "/addarticle" }, method = RequestMethod.POST, consumes="application/json")
	public Article insertArticle(@RequestBody Article article) {
		System.out.println("DANS ControllerArticle.insertArticle" + article);
		Article ArticleSaved = hServiceArticle.insertArticle(article);
		return ArticleSaved;
	}

	//Requête pour update une article : http://localhost:8080/updatearticle/70 +
	//json Postman: {"description": "Chaise", "marque": "Protype", "prixunitaire": 100.00}
	@PutMapping(value = {"/updatearticle/{id}"}, consumes="application/json", produces = "application/json")
	public void updateArticle(@PathVariable Integer id, @RequestBody Article article){
		System.out.println("DANS ControllerArticle.updateArticle 1 "+ id);
		Optional <Article> villeRequest = hServiceArticle.getServiceArticleById(id);
		System.out.println("DANS ControllerArticle.updateArticle 2" + villeRequest);
		Article articleResponse = villeRequest.get();
		articleResponse.setDescription(article.getDescription());
		articleResponse.setMarque(article.getMarque());
		articleResponse.setPrixunitaire(article.getPrixunitaire());
		hServiceArticle.updateArticle(articleResponse);
	}

}
