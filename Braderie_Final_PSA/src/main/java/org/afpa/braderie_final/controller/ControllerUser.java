/**
 * 
 */
package org.afpa.braderie_final.controller;

/**
 * @author samia
 *
 */
import java.util.List;
import java.util.Optional;

import org.afpa.braderie_final.bean.Panier;
import org.afpa.braderie_final.bean.User;
import org.afpa.braderie_final.service.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

/**
 * Le contrôleur utilise le service dans lequel les traitements métiers sont délégués
 * GET /users nous renvoie la liste des users
 * GET /users/{iduser} nous renvoie le user d'index iduser
 * POST /users/{iduser} nous permet de créer le user d'index iduser
 * PUT /users/{iduser} nous permet de modifier le user d'index iduser
 * DELETE /users/{iduser} nous permet de supprimer le user d'index iduser
 * cette norme est une norme type
 * @author samia
 *
 */
/**
 * Contrôleur gérant les interactions sur les users
 * @author samia
 *
 */
@RestController
@RequestMapping("/users")
public class ControllerUser {
	@Autowired
	private ServiceUser userService;
	
	/**
	 * Méthode qui retourne un user
	 * @param iduser id du user
	 * @return
	 */
	@ApiOperation(value="Retourne un id identifié par un iduser.")
	@GetMapping("/findById")
	public Optional<User> findById(@RequestParam(value="iduser", defaultValue="") Integer id) {
		return userService.findById(id);	
	}
	
	/**
	 * Methode qui retourne la liste des users
	 * @return
	 */
	@ApiOperation(value="Retourne la liste des users.")
	@GetMapping("/findAll")
	public List<User> findAll(){
		return userService.findAll();
	}
	
	/**
	 * Méthode ajoute un user
	 * @param login login du user
	 * @param pass mot de passe du user
	 */
	@ApiOperation(value="Ajoute un user.")
	@PostMapping("/postUser")
	public User add(@RequestBody User monUser) {
		return userService.add(monUser);	
	}
	
	/**
	 * Methode qui modifie un user
	 * @param id identifiant technique du user à modifier
	 * @param iduser id du user à modifier 
	 * @param pass mot de passe du user à modifier
	 * @param description nom de l'article du user rattaché au user
	 * @param idpanier nom du panier rattaché user
	 */
	@ApiOperation(value="Modifie un user.")
	@PutMapping("/putUser")
	public User update (@RequestBody User monUser) {
		return userService.add (monUser);
	
	}
	
	/**
	 * Methode supprimant un user
	 * @param id identifiant technique du user à supprimer
	 */
	@ApiOperation(value="Supprime un user.")
	@DeleteMapping("/deleteUser")
	public void delete (User monUser) {
		userService.remove(monUser);
	}
	
	
}
