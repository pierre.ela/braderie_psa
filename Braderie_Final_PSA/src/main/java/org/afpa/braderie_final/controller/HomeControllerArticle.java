package org.afpa.braderie_final.controller;

import org.afpa.braderie_final.service.ServiceArticle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeControllerArticle {
	@Autowired
	public ServiceArticle hServiceArticle; 

	
	@GetMapping("/vuearticle")
	public String getInternationalPage(Model model) {
		model.addAttribute("article", hServiceArticle.serviceListeArticle());
		return "vuearticle";
	}

}

