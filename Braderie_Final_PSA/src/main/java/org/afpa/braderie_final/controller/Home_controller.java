package org.afpa.braderie_final.controller;

import javax.servlet.http.HttpSession;

import org.afpa.braderie_final.bean.User;
import org.afpa.braderie_final.service.ServicePanier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Home_controller {
	@Autowired
	public ServicePanier servicePanier; 
	
	@RequestMapping("/")
	public  String home() {
		System.out.println("ici");
		return "index";
	}
	
	@RequestMapping("/panier/")
	public String panier(Model model, HttpSession maSession) {
		User monUser=(User) maSession.getAttribute("user");
		model.addAttribute("total", ServicePanier.totalPanier(servicePanier.findByIduser(monUser)));
		model.addAttribute("paniers", servicePanier.findByIduser(monUser));
		return "index";
	}

}
