package org.afpa.braderie_final.controller;

import org.afpa.braderie_final.bean.Panier;
import org.afpa.braderie_final.bean.User;
import org.afpa.braderie_final.service.ServicePanier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller to call differents methods according to URL
 * @author Pierre
 * 
 * 
 */
@RestController
public class ControllerPanier {


	@Autowired
	public ServicePanier servicePanier; 

	/**
	 * @return an Iterable containing all 'Panier'
	 */
	@GetMapping("/panier/findall")  
	public Iterable<Panier> panierFindAll() {
		return servicePanier.findAll();
		//return "hello user";
	}
	/**
	 * @param monPanier : Panier to delete
	 */
	@DeleteMapping(value = "/deletePanier")
	public void deletePanier(@RequestBody Panier monPanier) {
		servicePanier.delete(monPanier);
	}
	/**
	 * @param monPanier : Panier to insert or update
	 * @return
	 */
	@PutMapping(value = "/putPanier") //Insert
	public Panier putPanier(@RequestBody Panier monPanier) {
		return servicePanier.put(monPanier);
	}
	
	/**
	 * @param monPanier : Panier to inser or update
	 * @return Panier up to date
	 */
	@PostMapping(value = "/postPanier") //Insert
	public Panier postPanier(@RequestBody Panier monPanier) {
		return servicePanier.put(monPanier);
	}
	/**
	 * @param monUser : Current User
	 * @return : All panier of current user
	 */
	@GetMapping("/panier/findbyidUser")  
	public Iterable<Panier> panierFindByIduser(User monUser) {
		return servicePanier.findByIduser(monUser);
		
	}
	
	@PostMapping(value="/panier/updateQtePanier")
	public RedirectView  updateQtePanier(@RequestParam(name="id") Integer id, @RequestParam(name="qte") Integer qte ) {
		Panier monPanier= servicePanier.findByIDpanier(id).get();
		monPanier.setQuantite(qte);
		servicePanier.put(monPanier);
		return new RedirectView("/panier/");
	}
	
	@PostMapping(value="/panier/deletePanier")
	public RedirectView  deletePanier(@RequestParam(name="id") Integer id) {
		Panier monPanier= servicePanier.findByIDpanier(id).get();
		
		servicePanier.delete(monPanier);
		return new RedirectView("/panier/");
	}

}
