/**
 * 
 */
package org.afpa.braderie_final.service;

import java.util.Optional;

import org.afpa.braderie_final.bean.Article;
import org.afpa.braderie_final.repository.ArticleRepository;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author alexo
 *
 */

@Service
public class ServiceArticle {

	
//	@Autowired
//	private Logger logger;
	
	 @Autowired 
	 private ArticleRepository articleRepository;
	
	 private ArticleRepository getvilleRepository() {
		return articleRepository;
	}

	/**
	 * @param articleRepository
	 */
	public ServiceArticle(ArticleRepository articleRepository) {
		super();
		this.articleRepository = articleRepository;
	}
	 
	//Donne une liste des Articles de la BDD
	public Iterable<Article> serviceListeArticle() {
		Iterable<Article> list = articleRepository.findAll();
		list.forEach(article -> 
		System.out.println(article.getIdarticle()+" "
				+article.getDescription()+" "
				+article.getMarque()+" "
				+article.getPrixunitaire()));
		System.out.println("DANS ServiceArticle.serviceListeArticle \n" 
		+ list 
		+"\nNombre de article : "
		+ articleRepository.count()
		);
		return list;
	}
	
	//Trouve une Article par son ID
	public Optional <Article> getServiceArticleById(Integer id) {
		System.out.println("DANS ServiceArticle.getServiceArticleById");
		return articleRepository.findById(id);	
	}

	//Insere une Article dans la BDD
	public Article insertArticle(Article article) {
		System.out.println("DANS ServiceArticle.insertArticle");
		articleRepository.save(article);		
		return article;
	}
	
	//Update une Article dans la BDD
	public Article updateArticle(Article article) {
		System.out.println("DANS ServiceArticle.updateArticle");
		articleRepository.save(article);		
		return article;
	}
	
	//Supprime une Article dans la BDD
	public void deleteArticleById(Integer id) {
		System.out.println("DANS ServiceArticle.deleteArticleById");
		articleRepository.deleteById(id);
	}
	
}
