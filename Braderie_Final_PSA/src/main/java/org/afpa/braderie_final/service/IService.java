package org.afpa.braderie_final.service;

import java.util.*;

import org.afpa.braderie_final.bean.User;

/**
 * @author samia
 *
 */

public interface IService<T> {
	public void add(T obj);
	
	public void remove(Integer i);
	
	public T find(Integer i);
	
	public List<T> findAll();

	User update(User obj);
}
