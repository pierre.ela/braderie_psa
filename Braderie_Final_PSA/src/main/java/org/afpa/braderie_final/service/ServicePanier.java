package org.afpa.braderie_final.service;

import java.util.Optional;

import org.afpa.braderie_final.bean.Panier;
import org.afpa.braderie_final.bean.User;
import org.afpa.braderie_final.repository.PanierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Pierre
 *
 */
@Service (value="ServicePanier")
public class ServicePanier {


	@Autowired
	PanierRepository daoPanier ;
	
	

	/**
	 * @return Iterable <Panier>
	 */
	public Iterable<Panier> findAll() {
		return daoPanier.findAll();
		
	}

	/**
	 * @param Panier
	 */
	public void add(Panier monPanier) {
		daoPanier.save(monPanier);
		
	}
	
	/**
	 * @param Panier
	 */
	public void delete(Panier monPanier) {
		daoPanier.delete(monPanier);
	}
	
	/**
	 * @param Panier
	 * @return Panier
	 */
	public Panier put(Panier monPanier) {
		return daoPanier.save(monPanier);
	}

	/**
	 * @param monUser
	 * @return : Iterable containing all Panier
	 */
	public Iterable<Panier> findByIduser(User monUser) {
		
		return daoPanier.findByIduserOrderByArticleAsc(monUser);
	}
	
	public Optional<Panier> findByIDpanier(Integer idPanier) {
		return daoPanier.findById(idPanier);
	}
	
	public static Double totalPanier (Iterable<Panier> monPanier) {
		Double total=(double) 0;
		for(Panier curr : monPanier) {
			total+=curr.getQuantite()*curr.getArticle().getPrixunitaire();
		}
		return total;
	}

}
