/**
 * 
 */
package org.afpa.braderie_final.service;

import java.util.*;

import org.afpa.braderie_final.bean.User;
import org.afpa.braderie_final.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author samia
 *
 */
@Service(value="ServiceUser")
@Transactional
public class ServiceUser {
	
	@Autowired
	private UserRepository userRepository;
	
	
	public User add(User obj) {
		return userRepository.save(obj);
	}
	
	
	public void remove(User id) {
		userRepository.delete(id);
		System.out.println("user n° " +id + "effacé");
		
	}
	
	public List<User> findAll() {
		return userRepository.findAll();
	}
	

	public User update(User obj) {
		return userRepository.save (obj);
			}

	public Optional<User> findById(Integer id) {
		return userRepository.findById(id);
		
	}

	public boolean IsValidlogon (String login, String password) {
		if (userRepository.isValidLogon( login, password)==1) {
			return true;
		}
		return false;
	}

	public User findByLogin (String login) {
		return userRepository.findByLogin(login);

	}

}
