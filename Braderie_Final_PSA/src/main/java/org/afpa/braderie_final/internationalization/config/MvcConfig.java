/**
 * 
 */
package org.afpa.braderie_final.internationalization.config;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * @author alexo
 *
 *i18n: http://localhost:8080/international
 *
 *LocaleResolver: pour que notre application puisse déterminer la locale actuellement 
 *utilisée, nous devons ajouter un bean LocaleResolver :
 *L'interface LocaleResolver possède des implémentations qui déterminent la locale 
 *actuelle en fonction de la session, des cookies, de l'en-tête Accept-Language ou d'une 
 *valeur fixe.
 *Dans notre exemple, nous avons utilisé le résolveur basé sur la session 
 *SessionLocaleResolver et défini une locale par défaut avec la valeur US.
 *
 *LocaleChangeInterceptor ajoute un bean intercepteur qui basculera vers une nouvelle 
 *locale en fonction de la valeur du paramètre lang ajouté à une requête :
 *
 *Afin de prendre effet, ce bean doit être ajouté au registre des intercepteurs de 
 *l'application. Pour ce faire, notre classe @Configuration doit implémenter l'interface 
 *WebMvcConfigurer et surcharger la méthode addInterceptors() :
 *
 *Définition des sources de messages, par défaut, une application Spring Boot recherche les 
 *fichiers de messages contenant les clés et les valeurs d'internationalisation dans le 
 *dossier src/main/resources. Le fichier de la locale par défaut s'appelle 
 *messages.properties, et les fichiers de chaque locale s'appellent messages_XX.properties, 
 *où XX est le code de la locale.
 *
 *Les clés des valeurs qui seront localisées doivent être les mêmes dans chaque fichier, 
 *avec des valeurs appropriées à la langue à laquelle elles correspondent. 
 *Si une clé n'existe pas dans une certaine locale demandée, l'application se rabattra sur 
 *la valeur locale par défaut.Définissons un fichier de messages par défaut pour la langue 
 *anglaise, appelé messages.properties
 *
 *Créons un mappage de contrôleur qui renverra une simple page HTML appelée international.html 
 *que nous voulons voir dans deux langues différentes.
 *
 *Puisque nous utilisons thymeleaf pour afficher la page HTML, les valeurs spécifiques 
 *locales seront accessibles en utilisant les clés avec la syntaxe #{key} :
 *
 *Si nous voulons accéder à la page avec les deux locales différentes, nous devons ajouter 
 *le paramètre lang à l'URL de la forme : /international?lang=fr.
 *Si aucun paramètre lang n'est présent dans l'URL, l'application utilisera la locale par 
 *défaut, dans notre cas la locale US.
 *Ajoutons une liste déroulante à notre page HTML avec les deux locales dont les noms sont 
 *également localisés dans nos fichiers de propriétés.
 *
 *Nous pouvons ensuite ajouter un script jQuery qui appellera l'URL /international avec le 
 *paramètre de langue respectif en fonction de l'option de liste déroulante sélectionnée.
 *
 *En fonction de la locale sélectionnée, nous verrons la page en anglais ou en français lors 
 *de l'exécution de l'application.
 */


@Configuration
@ComponentScan(basePackages = "org.afpa.braderie_final.internationalization.config")
public class MvcConfig implements WebMvcConfigurer {

	//Local résolveur basé sur la session et par défaut sur la langue US.
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.FRANCE);
        return slr;
    }

    
    //Bascule entre les langues
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    
    //Ajoute au registre des intercepteurs de l'application.
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }
}