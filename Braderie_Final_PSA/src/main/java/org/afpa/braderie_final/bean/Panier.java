/**
 * 
 */
package org.afpa.braderie_final.bean;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author 31010-83-02
 *
 */

/**
 * @author Pierre
 *
 */
@Entity
@Table(name = "t_panier")
public class Panier {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	Integer idpanier;
	Integer quantite;
	@OneToOne ( cascade= {CascadeType.PERSIST})
	@JoinColumn( name="iduser" )
	private User iduser;
	@OneToOne ( cascade= {CascadeType.PERSIST})
	@JoinColumn( name="idarticle" )
	private Article article;
		


	/**
	 * @return the idpanier
	 */
	public Integer getIdpanier() {
		return idpanier;
	}
	/**
	 * @param idpanier the idpanier to set
	 */
	public void setIdpanier(Integer idpanier) {
		this.idpanier = idpanier;
	}
	/**
	 * @return the quantité
	 */
	public Integer getQuantite() {
		return quantite;
	}
	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}
	/**
	 * @return the iduser
	 */
	public User getUser() {
		return this.iduser;
	}
	
	/**
	 * @param monUser user to set into current panier
	 */
	public void setUser(User monUser) {
		this.iduser = monUser;
	}
	
	/**
	 * @return Article : current Article
	 */
	public Article getArticle() {
		return article;
	}
	
	/**
	 * @param idarticle Article to set into current Panier
	 */
	public void setArticle(Article idarticle) {
		this.article = idarticle;
	}

}
