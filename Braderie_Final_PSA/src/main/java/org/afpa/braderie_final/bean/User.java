/**
 * 
 */
package org.afpa.braderie_final.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe qui vient mapper une table de la base de données
 * un objet représente une ligne de la base de données
 * @author samia
 *
 */
@Entity
@Table(name="t_user")
public class User {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer iduser;
	private String login;
	private String pass;
	private Integer nbconnexion;
	
	/**
	 * 
	 */
	public User() {
		super();
	}
	/**
	 * @return the iduser
	 */
	public Integer getIduser() {
		return iduser;
	}
	/**
	 * @param iduser the iduser to set
	 */
	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}
	/**
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}
	/**
	 * @return the nbconnexion
	 */
	public Integer getNbconnexion() {
		return nbconnexion;
	}
	/**
	 * @param nbconnexion the nbconnexion to set
	 */
	public void setNbconnexion(Integer nbconnexion) {
		this.nbconnexion = nbconnexion;
	}
	/**
	 * @param iduser
	 * @param login
	 * @param pass
	 * @param nbconnexion
	 */
	public User(Integer iduser, String login, String pass, Integer nbconnexion) {
		super();
		this.iduser = iduser;
		this.login = login;
		this.pass = pass;
		this.nbconnexion = nbconnexion;
	}
	@Override
	public String toString() {
		return "User [iduser=" + iduser + ", login=" + login + ", pass=" + pass + ", nbconnexion=" + nbconnexion + "]";
	}
	
	

}