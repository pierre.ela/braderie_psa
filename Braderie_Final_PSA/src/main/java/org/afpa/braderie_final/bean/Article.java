/**
 * 
 */
package org.afpa.braderie_final.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.afpa.util.JPAUtil;

/**
 * @author alexo
 *
 */
@Entity
@Table(name = "t_article")
public class Article {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idarticle", unique=true, nullable=false)
	Integer idarticle;
	
	@Column(name="description")
	String description;
	
	@Column(name="prixunitaire")
	Double prixunitaire;
	
	@Column(name="marque")
	String marque;

	

	/**
	 * 
	 */
	public Article() {
		super();
	}



	/**
	 * @param idarticle
	 * @param description
	 * @param prixunitaire
	 * @param marque
	 */
	public Article(Integer idarticle, String description, Double prixunitaire, String marque) {
		super();
		this.idarticle = idarticle;
		this.description = description;
		this.prixunitaire = prixunitaire;
		this.marque = marque;
	}

	

	@Override
	public String toString() {
		return "Article [idarticle=" + idarticle + ", description=" + description + ", prixunitaire=" + prixunitaire
				+ ", marque=" + marque + "]";
	}

	

	/**
	 * @return the idarticle
	 */
	public Integer getIdarticle() {
		return idarticle;
	}



	/**
	 * @param idarticle the idarticle to set
	 */
	public void setIdarticle(Integer idarticle) {
		this.idarticle = idarticle;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * @return the prixunitaire
	 */
	public Double getPrixunitaire() {
		return prixunitaire;
	}



	/**
	 * @param prixunitaire the prixunitaire to set
	 */
	public void setPrixunitaire(Double prixunitaire) {
		this.prixunitaire = prixunitaire;
	}



	/**
	 * @return the marque
	 */
	public String getMarque() {
		return marque;
	}



	/**
	 * @param marque the marque to set
	 */
	public void setMarque(String marque) {
		this.marque = marque;
	}



//	public Map<Integer, Article>  getAll(){
//		Map<Integer, Article> collection = new HashMap<Integer, Article> () ;
//		
//		EntityManager monManager=JPAUtil.getEntityManager();
//		List<Article> allArticle	= monManager.createQuery("from Article", Article.class).getResultList();
//
//		for (Article a : allArticle) {
//			collection.put(a.idarticle, a);
//		}
//				
//		return collection;
//	}


}
