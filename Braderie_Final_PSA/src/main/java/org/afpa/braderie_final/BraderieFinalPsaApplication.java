package org.afpa.braderie_final;


/*
 * 
 * http://localhost:8080/swagger-ui.html
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableSwagger2
@SpringBootApplication
public class BraderieFinalPsaApplication {
	 
	public static void main(String[] args) {
		SpringApplication.run(BraderieFinalPsaApplication.class, args);
	}

	
}
