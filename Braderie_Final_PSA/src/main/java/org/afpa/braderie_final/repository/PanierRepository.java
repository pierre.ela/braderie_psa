package org.afpa.braderie_final.repository;

import org.afpa.braderie_final.bean.Panier;
import org.afpa.braderie_final.bean.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Pierre
 *
 */
public interface PanierRepository extends CrudRepository <Panier, Integer> {

	

	Iterable<Panier> findByIduserOrderByArticleAsc(User iduser);


}
