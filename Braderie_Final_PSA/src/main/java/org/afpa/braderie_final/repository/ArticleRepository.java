/**
 * 
 */
package org.afpa.braderie_final.repository;


import org.afpa.braderie_final.bean.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



/**
 * @author alexo
 *
 */

@Repository
public interface ArticleRepository extends CrudRepository<Article, Integer>{

}
