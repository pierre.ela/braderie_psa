/**
 * 
 */
package org.afpa.braderie_final.repository;

import org.afpa.braderie_final.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author samia
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	@Query(value = "Select IsValidlogon( :username, :password);", nativeQuery = true)
    public Integer isValidLogon( @Param("username") String login, @Param("password") String pass);

	public User findByLogin(String login);


}