package org.afpa.braderie_final;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.afpa.braderie_final.bean.Article;
import org.afpa.braderie_final.bean.Panier;
import org.afpa.braderie_final.bean.User;
import org.afpa.braderie_final.service.ServicePanier;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
class BraderieFinalPsaApplicationTests {

	@Autowired
	private ServicePanier servPanier;
	
	@Test
	void contextLoads() {
	}
	
	
	//Test Insert
	@Test
	public void testCreate() throws Exception{
		User monUser=new User(null, "bob", "marley", null);
		Article monArticle=new Article(null, "monArticle", 2D, "maMarque");
		Panier monPanier=new Panier ();
		
		monPanier.setArticle(monArticle);
		monPanier.setUser(monUser);
		monPanier.setQuantite(1);
		
		Panier monPanierRepo= servPanier.put(monPanier);
		assertEquals(monPanierRepo.getUser(), monPanier.getUser());
		assertTrue(monPanierRepo.getIdpanier()!=null);
		assertEquals(monPanierRepo.getArticle(), monPanier.getArticle());
	}
	
	// Test Update
	@Test
	public void testUpdate() throws Exception{
		User monUser=new User(null, "bob", "marley", null);
		Article monArticle=new Article(null, "monArticle", 2D, "maMarque");
		Panier monPanier=new Panier ();
		
		monPanier.setArticle(monArticle);
		monPanier.setUser(monUser);
		monPanier.setQuantite(1);
		Panier monPanierRepo= servPanier.put(monPanier);
		
		// Modification du panier
		monPanier.setQuantite(10);
		monPanierRepo.setQuantite(monPanier.getQuantite());
		
		// Test
		assertEquals(monPanierRepo.getArticle(), monPanier.getArticle());
		assertEquals(monPanierRepo.getQuantite(), monPanier.getQuantite());
	}
	
	// Test delete
	@Test
	public void testDelete() throws Exception{
		User monUser=new User(null, "bob", "marley", null);
		Article monArticle=new Article(null, "monArticle", 2D, "maMarque");
		Panier monPanier=new Panier ();
		
		monPanier.setArticle(monArticle);
		monPanier.setUser(monUser);
		monPanier.setQuantite(1);
		Panier monPanierRepo= servPanier.put(monPanier);
		
		// On delete
		servPanier.delete(monPanier);
		assertEquals(servPanier.findByIDpanier(monPanierRepo.getIdpanier()),Optional.empty());
		
		
		
	}
	
	

}
